#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Editor'
SITENAME = 'Free - Hur'
SITEURL = 'https://uak.gitlab.io/hur-website-single-freelancer/'

PATH = 'website/content'

TIMEZONE = 'Asia/Amman'

DEFAULT_DATE = 'fs'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

SEARCH_MODE = "output"

#THEME = "pelican-themes/flex"


# Blogroll
# ~ LINKS = (
         # ~ ('النص البرمجي', 'https://uak.gitlab.io/hur_website/'),
         # ~ ('لنقاش المشروع', 'https://matrix.to/#/!sLoVgTyaAsGhBCWtkw:kde.org?via=kde.org&via=tchncs.de&via=matrix.org'),
        # ~ )
LINKS = (
         ('Source Code', 'https://gitlab.com/uak/hur-website-single-freelancer'),
         ('Project Discussion', 'https://t.me/hur_project'),
        )


# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

STORK_INPUT_OPTIONS = {"url_prefix": SITEURL}
